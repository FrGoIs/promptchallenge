const jsonForm = document.getElementById("json-form");
const jsonFile = document.getElementById("json-file");
const jsonFormContainer = document.getElementById("json-forms-container");
const generatedHTMLElementsArray = [];
// Regular expression for validation
const nameRegex = /^[a-zA-Z\s]+$/;
const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const phoneRegex = /^\d{10}$/;
const zipRegex = /^\d{5}$/;
jsonForm.addEventListener("submit", (event) => {
    event.preventDefault();
    const files = jsonFile.files;
    for (let i = 0; i < files.length; i++) {
        const file = files[i];
        const reader = new FileReader();

        reader.addEventListener("load", (event) => {
            const result = event.target.result;
            const data = JSON.parse(result);
            // remove extension from file name
            const formName = file.name.split('.').slice(0, -1).join('.');
            displayForm(data, formName);
        });

        reader.readAsText(file);
    }
});

function displayForm(data, formName) {
    let html = `<h2>${formName}</h2>\n<form id="${formName}">\n`;
    for (const key in data) {
        if (data.hasOwnProperty(key)) {
            html += `<label for="${key}">${key}</label><br>\n`;
            if (typeof data[key] === 'object') {
                // If the value is an object, create a fieldset and call createInputFieldsFromObject
                html += `<fieldset><legend>${key}</legend>`;
                html += createInputFieldsFromObject(data[key]);
                html += `</fieldset>`;
            } else {
                // If the value is not an object, create a simple input field
                html += `<input type="text" id="${key}" name="${key}" value="${data[key]}"><br>\n`;
            }
        }
    }
    html += `<br><button type="submit">Save Data</button>\n`;
    html += `</form>`;
    jsonFormContainer.innerHTML += html;
    const newForm = document.getElementById(formName)
    generatedHTMLElementsArray.push(newForm);
    generatedHTMLElementsArray[generatedHTMLElementsArray.length -1].addEventListener("submit", (event) => {
        event.preventDefault();
        const formData = new FormData(newForm);
        saveData(formData);
    })
}

function createInputFieldsFromObject(obj) {
    let html = '';
    Object.keys(obj).forEach(key => {
        const value = obj[key];
        if (typeof value === 'object') {
            // If the value is an object, recursively create input fields for its properties
            html += `<fieldset><legend>${key}</legend>`;
            html += createInputFieldsFromObject(value);
            html += `</fieldset>`;
        } else {
            // If the value is not an object, create a simple input field
            html += `<label for="${key}">${key}</label><br>\n`;
            html += `<input type="text" id="${key}" name="${key}" value="${value}"><br>\n`;
        }
    });
    return html;
}


// Function to save data
function saveData(formData) {
    fetch('/api/save-data', {
        method: 'POST',
        body: formData
    })
        .then(response => {
            if (response.ok) {
                alert("Data saved successfully!");
            } else {
                alert("Failed to save data. (set up api/save-data route)");
            }
        })
        .catch(error => {
            alert("An error occurred while saving the data.");
            console.error(error);
        });
}

function createFormFromJson(json) {
    // Parse the JSON
    const data = JSON.parse(json);

    // Clear any existing form elements
    const container = document.getElementById('json-form-container');
    container.innerHTML = '';

    // Create the form element
    const form = document.createElement('form');
    container.appendChild(form);

    // Create the input fields for each property in the JSON object
    Object.keys(data).forEach(key => {
        const value = data[key];
        if (typeof value === 'object') {
            // If the value is an object, recursively create input fields for its properties
            const fieldset = document.createElement('fieldset');
            const legend = document.createElement('legend');
            legend.innerHTML = key;
            fieldset.appendChild(legend);
            form.appendChild(fieldset);
            createInputFieldsFromObject(value, fieldset);
        } else {
            // If the value is not an object, create a simple input field
            const label = document.createElement('label');
            label.innerHTML = key;
            const input = document.createElement('input');
            input.setAttribute('name', key);
            input.setAttribute('value', value);
            form.appendChild(label);
            form.appendChild(input);
        }
    });
}
