# Prompts Used

1. Give me the html and css code for a website that takes a JSONFile. The styling should be filled with organic shapes, curves, minimalist color scheme and bold typography
2. I need an input field to provide the json file
3. The page will take an arbitrary JSON and convert it to an HTML Form that can save the data in a Database
4. Give me an example json file to test the page.
5. When I provided that file it did not convert it to a form.
6. Give a high-tech business styling to the generated form.
7. Add Javascript validation to the form and prevent sql injection attacks
8. Create an organic, minimalistic color scheme and apply it to what we have so far.
9. Here is a list of the colors you should draw the color scheme from: Primary Blue - #0070BB
   Medium Blue - #27A1F2
   Light Blue - #88CFFF
   White - #FFFFFF
   Dark Grey - #D4DADF
   Light Grey - #EEF1F4
   Icon Grey - #B4BBC1
   Text Grey - #4A4C4E
   Black - #000000
   Dark Green - #219653
   Light Green - #27AE60
   Red - #EB5757
   Yellow - #F2C94C
   Orange - #F2994A
10. the page should be able to generate input fields from nested objects
11. add the ability to upload more than one file and generate more than one form
12. Provide the adequate js code from this: (here I provided what I had so far on the JS side but the resulting code did not work)  
